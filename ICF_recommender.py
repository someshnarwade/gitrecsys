import pandas as pd
from scipy.spatial.distance import cosine

data = pd.read_csv('rating1.csv')
#print(data.head(6))
data_germany = data.drop('-1', 1)
# print(data_germany.head().ix[:,0:8])
data_ibs = pd.DataFrame(index=data_germany.columns,columns=data_germany.columns)
for i in range(0,len(data_ibs.columns)) :
    # Loop through the columns for each column
    for j in range(0,len(data_ibs.columns)) :
      # Fill in placeholder with cosine similarities
      data_ibs.ix[i,j] = 1-cosine(data_germany.ix[:,i],data_germany.ix[:,j])

data_neighbours = pd.DataFrame(index=data_ibs.columns,columns=range(1,11))

for i in range(0,len(data_ibs.columns)):
    data_neighbours.ix[i,:10] = data_ibs.ix[0:,i].order(ascending=False)[:10].index

print(data_neighbours.head().ix[:10,0:6])