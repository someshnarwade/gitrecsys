import pandas as pd
import numpy as np
import collections
from collections import OrderedDict
from operator import itemgetter
import timeit

start = timeit.default_timer()

df = pd.read_csv("watchers.csv",names = ["project_id","user_id","timestamp"]) 

projects = list(df['project_id'])
users = list(df['user_id'])

usr_counter = collections.Counter(users)
d = OrderedDict(sorted(usr_counter.items(), key=itemgetter(1), reverse = True))
user100 = {k:v for k,v in d.items() if v > 2120}
user_list = list(user100.keys())

pro_counter = collections.Counter(projects)
d1 = OrderedDict(sorted(pro_counter.items(), key=itemgetter(1), reverse = True))
pro100 = {k:v for k,v in d1.items() if v > 5830}
pro_list = list(pro100.keys())


temp = []
for i in user_list:
	temp.append(np.where(df['user_id'] == i))
	
project_ids = []
for i in temp:
	for j in i:
		project_ids.append(df['project_id'].values[j]) 

'''
s = set(project_ids[0])
for i in range(1,len(project_ids)):
	k = set(project_ids[i])
	s = s.union(k)
	
rating_list = [0] * 300

ulist = pd.Series(user_list)
plist = pd.Series(pro_list)
rlist = pd.Series(rating_list)

columns = ['project_id','user_id','ratings']

newdf = pd.DataFrame(columns=columns)

newdf['project_id'] = plist.values
newdf['user_id'] = ulist.values
newdf['ratings'] = rlist.values

#print (newdf.head())

R_df = newdf.pivot(index = 'user_id', columns ='project_id', values = 'ratings').fillna(0)

print (R_df.head())

R_df.to_csv("rating0.csv", encoding='utf-8')
'''



with open("rating0.csv") as f:
    lis=[list(map(int,line.strip().split(","))) for line in f]        # create a list of lists

for j in range(0,len(project_ids)):
	for i in project_ids[j]:
		try:
			ind = (lis[0]).index(i)
			lis[j+1][ind] = 1
		except:
			continue

df2 = pd.DataFrame(lis)
	
df2.to_csv("rating1.csv", encoding='utf-8', index=False)

stop = timeit.default_timer()

print('Time: ', stop - start)
